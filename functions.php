<?php
/*
* Add Style CSS
*/
function additional_custom_styles() {
    /*Enqueue The Styles*/
    wp_enqueue_style( 'themename', get_template_directory_uri() . '/css/bootstrap.min.css' );
}
add_action( 'wp_enqueue_scripts', 'additional_custom_styles' );

?>