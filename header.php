<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package Fiberwerx
 * @subpackage Fiberwerx 1.0
 * @since 2016
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
</head>
<body>

<header>
    <div class="row">
	    <div class="col-lg-12">
	        <div class="container">
	        	<nav class="navbar">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                       <a class="navbar-brand" href="#">
                         <h1>TITle</h1>
                       </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav navbar-right" id="LG-screen-menu"> 
                        <li class="active"><a href="./">HOME <span class="sr-only">(current)</span></a></li>
                        <li><a href="../navbar-static-top/">ABOUT US</a></li>
                        <li><a href="../navbar-fixed-top/">SERVICES</a></li>
                        <li><a href="../navbar-fixed-top/">CONTACT US</a></li>
                      </ul>
                    </div><!--/.nav-collapse -->
                  </div><!--/.container-fluid -->
                </nav>
	        </div>
	    </div> 
	</div>
</header>

<section>
     <div class="col-lg-12">
     	 
     </div>
</section>

